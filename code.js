const [...allbtns] = document.querySelectorAll(".button");
const display = document.querySelector('.display input')
let firstNumber = '';
let secondNumber = '';
let math = '';

function saveData(data) {
    if (data.search(/\d/) !== -1 && math.length == 0) {
        firstNumber += data;
        show(firstNumber)
    }
    else if(data.search(/\d/) !== -1 && math.length !== 0) {
        secondNumber += data;
        show(secondNumber)
    }
else if(data.search(/[-+*/]/) !== -1) {
    math = data;
}

}
function show(number) {
    display.value = number;
}

allbtns.forEach(function (element) {
    element.addEventListener('click', function () {
        saveData(element.value)

    })
})